# SpectreEvents

This is a sample Ruby on Rails project for WhiteSpectre interview evaluation. It's not a production ready app, nor intended to use in production.

### How to run locally
* Make sure you've
  * Ruby (Preferably `v2.3.1`)
  * Bundler
  * Rails (Preferably `5.rc1`)
* Go the project directory from terminal.
* Run `bundle install`
* Migrate database `rails db:migrate`
* Run the server: `rails s`

The API route will be available at `/events` path.

You may want to run the specs: `rspec`.