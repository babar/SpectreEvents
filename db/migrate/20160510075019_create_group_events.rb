class CreateGroupEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :group_events do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.text :description
      t.string :location
      t.date :start_date
      t.date :end_date
      t.integer :duration
      t.integer :status

      t.timestamps

      t.index :status
    end
  end
end
