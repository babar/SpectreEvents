class EventsController < ApplicationController
  before_action :set_event, only: [:show, :update, :destroy]

  def index
    @events = GroupEvent.all

    render json: @events
  end

  def show
    render json: @event
  end

  def create
    @event = GroupEvent.new(event_params)

    if @event.save
      render json: @event, status: :created, location: event_url(@event)
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  def update
    if @event.update(event_params)
      render json: @event
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @event.destroy
  end

  private
    def set_event
      @event = GroupEvent.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:user_id, :name, :description, :location, :start_date, :end_date, :duration, :status)
    end
end
