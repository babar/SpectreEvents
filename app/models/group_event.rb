class GroupEvent < ApplicationRecord
  belongs_to :user
  enum status: {draft: 0, published: 1, deleted: 2}

  # Note: This could be implemented with a gem!
  alias_method :really_delete!, :destroy
  alias_method :destroy, :deleted!

  validates_presence_of :name, :description, :location, :start_date, :end_date, if: 'published?'
  validate :whole_duration_days

  default_scope {where.not(status: :deleted)}

  before_validation :set_defaults, :set_end_date, :set_duration

  private
    def set_defaults
      self.start_date ||= Date.today
      self.status ||= :draft
    end

    def set_end_date
      if duration_changed?
        self.end_date = start_date + duration.days
      end
    end

    def set_duration
      self.duration = (end_date - start_date).to_i if end_date
    end

    def whole_duration_days
      if duration and (duration % 30) > 0
        errors.add(:duration, "can't be un whole!")
      end
    end
end
