require 'rails_helper'

describe EventsController, type: :routing do
  describe 'routing' do

    it 'routes to #index' do
      expect(get: '/events').to route_to('events#index')
    end

    it 'routes to #create' do
      expect(post: '/events').to route_to('events#create')
    end

    {show: 'get', update: 'put', destroy: 'delete'}.each do |action, type|
      it "routes to ##{action}" do
        expect("#{type}": '/events/1').to route_to("events##{action}", id: '1')
      end
    end

  end
end
