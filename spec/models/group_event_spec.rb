require 'rails_helper'

describe GroupEvent, type: :model do
  before do
    User.create(name: 'John Doe')
  end

  it 'can have name, description and location' do
    create_event
    expect(GroupEvent.count).to be(1)
  end

  it 'will not be published without all attributes' do
    e = new_event(status: :published)
    expect{e.save!}.to raise_error(ActiveRecord::RecordInvalid)
  end

  it 'can calculate duration from start and end date' do
    e = create_event(start_date: Date.today, end_date: Date.today + 30.days)
    expect(e.duration).to be(30)
  end

  it 'can set end date from duration' do
    e = create_event(start_date: Date.today, duration: 30)
    expect(e.end_date).to eq(Date.today+30.days)
    expect(e.duration).to be(30)
  end

  it 'supports soft delete' do
    e = create_event(name: 'A event', description: 'Agdum Bagdum', location: 'Mars', user_id: 1)
    id = e.id
    e.destroy
    expect(GroupEvent.unscoped.find(id)).not_to be_nil
  end

  it 'does not retrieve removed records by default' do
    create_event.destroy
    expect(GroupEvent.count).to be(0)
  end

  context '#really_delete!' do
    it 'can fully delete record from db' do
      create_event.really_delete!
      expect(GroupEvent.unscoped.count).to be(0)
    end
  end

  context '#duration' do
    it 'must be a whole number (multiplication of 30)' do
      expect{create_event(start_date: Date.today, duration: 31)}.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  # Note: Helper methods, this could be improved with factory girl
  def create_event(attributes = {})
    GroupEvent.create!(event_attrs(attributes))
  end

  def new_event(attributes = {})
    GroupEvent.new(event_attrs(attributes))
  end

  def event_attrs(attributes = {})
    {name: 'A event', description: 'Agdum Bagdum', location: 'Mars', user_id: 1}.merge(attributes)
  end
end
