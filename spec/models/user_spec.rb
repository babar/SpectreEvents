require 'rails_helper'

describe User, type: :model do
  it 'can save a user' do
    User.create(name: 'John Doe')
    expect(User.count).to be(1)
  end

  it 'must have a name' do
    expect{User.create!}.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Name can\'t be blank')
  end
end
