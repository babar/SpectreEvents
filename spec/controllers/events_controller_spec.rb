require 'rails_helper'

describe EventsController, type: :controller do
  before do
    User.create(name: 'John Doe')
  end

  let(:valid_params) {
    {name: 'A event', description: 'Agdum Bagdum', location: 'Mars', user_id: 1, start_date: Date.today, end_date: Date.today + 60.days}
  }

  let(:invalid_params) {
    {name: 'A event', description: 'Agdum Bagdum', location: 'Mars', user_id: 1}
  }

  describe 'GET index' do
    it 'assigns all events as @events' do
      event = GroupEvent.create! valid_params
      get :index, params: {}
      expect(JSON.parse(response.body).last['id']).to be(event.id)
    end
  end

  describe 'GET show' do
    it 'assigns the requested event as @event' do
      event = GroupEvent.create! valid_params
      get :show, params: {id: event.to_param}
      expect(JSON.parse(response.body)['id']).to be(event.id)
    end
  end

  describe 'POST create' do
    describe 'with valid params' do
      it 'creates a new Event' do
        expect {
          post 'create', params: {event: valid_params}
        }.to change(GroupEvent, :count).by(1)
      end

      it 'sends back a newly created event' do
        post :create, params: {event: valid_params}
        expect(JSON.parse(response.body)['name']).to eq(valid_params[:name])
      end
    end
  end

  describe 'PUT update' do
    describe 'with valid params' do
      let(:new_attributes) {
        valid_params.merge(name: 'Mula')
      }

      it 'updates the requested event' do
        event = GroupEvent.create! valid_params
        put :update, params: {id: event.to_param, event: new_attributes}
        expect(response.body).to include('Mula')
      end
    end
  end

  describe 'DELETE destroy' do
    it 'destroys the requested event' do
      event = GroupEvent.create! valid_params
      expect {
        delete :destroy, params: {id: event.to_param}
      }.to change(GroupEvent, :count).by(-1)
    end
  end

end
